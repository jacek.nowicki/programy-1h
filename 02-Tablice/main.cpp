#include <iostream>

using namespace std;

int main()
{
    int rozmiarTablicy;
    cin >> rozmiarTablicy;

    int tablica[rozmiarTablicy]; //Tutaj tworzymy
    tablica[0] = 25; //Zapisanie wartości do 0-go elementu
    cout << tablica[0] << endl; //Wyświetlanie wartości


    for (int i = 0; i < rozmiarTablicy; i++)
    {
        tablica[i] = i*2;
    }
    for (int i = 0; i< rozmiarTablicy; i++)
    {
        cout << tablica[i] << " ";
    }

    int tab2[5] = {1, 2, 3, 4, 5};
    int tab3[5] {1, 2, 3, 4, 5};
    int tab4[] = {1, 2, 3, 4, 5};
    int tab5[] {1, 2, 3, 4, 5};

    for (int i = 0; i < 5; i++)
    {
        cout << tab2[i] << "";
        cout << tab3[i] << "";
        cout << tab4[i] << "";
        cout << tab5[i] << endl;
    }

    int tab2D_1[10][20];
    int tab2D_2[2][2] = { {1, 2}, {3, 4} };

    for (int w = 0; w < 10; w++)
    {
        for (int k = 0; k < 20; k++)
        {
            tab2D_1[w][k] = w * k;
            cout << tab2D_1[w][k] << " ";
        }
        cout << endl;
    }



    return 0;
}
