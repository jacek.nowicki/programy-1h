#include <iostream>

using namespace std;

class Ksiazka {
public:
    string Tytul;
    int IloscStron;
    string Wydawnictwo;
    string Autor;
    string ISBN;
    string OdczytajDane()
    {
        return Autor + " " +
            Wydawnictwo + " " + Tytul;
    }

    Ksiazka()
    {

    }

    Ksiazka(string tytul, int iloscStron, string wydawnictwo,
            string autor, string isbn)
    {
        Tytul = tytul;
        IloscStron = iloscStron;
        Wydawnictwo = wydawnictwo;
        Autor = autor;
        ISBN = isbn;
    }

private:
    float cena;
    int iloscSlow;

    float CenaZaSlowo()
    {
        return cena / iloscSlow;
    }
};

int main()
{
    Ksiazka mojaKsiazka = Ksiazka();
    mojaKsiazka.Autor = "Boleslaw Prus";
    mojaKsiazka.Tytul = "Lalka";
    mojaKsiazka.Wydawnictwo = "Znak";
    mojaKsiazka.IloscStron = 500;
    mojaKsiazka.ISBN = "AAAA-BBBB-CCCC";
    cout << mojaKsiazka.OdczytajDane() << endl;

    Ksiazka mojaDruga = Ksiazka("Krzyzacy", 2000,
                                "Bolek i Lolek",
                                "Henryk Sienkiewicz",
                                "DDDD-EEEE");

    cout << mojaDruga.OdczytajDane() << endl;

    return 0;
}
