#include <iostream>

using namespace std;

int main()
{
    struct Pracownik
    {
        string imie;
        string nazwisko;
        int wiek;
    };

    Pracownik magazynier =
    {
        "Jan",
        "Kowalski",
        32
    };

    Pracownik ksiegowy
    {
        "Wojciech",
        "Maciaszczyk",
        45
    };

    Pracownik kadrowa;
    kadrowa.imie = "Anna";
    kadrowa.nazwisko = "Ryba";
    kadrowa.wiek = 35;

    Pracownik pracownicy[5];
    pracownicy[0].imie = "Jan";

    cout << kadrowa.imie << " " << kadrowa.nazwisko << " " << kadrowa.wiek << endl;

    cout << "Hello world!" << endl;
    return 0;
}
