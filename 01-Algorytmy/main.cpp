#include <iostream>

using namespace std;

int main()
{
    // Odczytywanie adres�w
    int liczba = 15;
    int tab[] = { 20, 25, 30 };

    cout << "Adres liczby: " << &liczba << endl;
    cout << "Adres tablicy: " << &tab << endl;
    cout << "Adres tab[0]: " << &tab[0] << endl;
    cout << "Adres tab[1]: " << &tab[1] << endl;
    cout << "==========================" << endl;
    // Wska�niki
    int liczba2 = 9;
    int *wsk_liczba2;
    wsk_liczba2 = &liczba2;

    cout << "Wartosc zmiennej: " << liczba2 << endl;
    cout << "Adres zmiennej: " << &liczba2 << endl;
    cout << "Wartosc wskaznika: " << *wsk_liczba2 << endl;
    cout << "Adres wskaznika: " << wsk_liczba2 << endl;

    int *wsk2_liczba2 = &liczba2;
    cout << "Wartosc drugiego wskaznika: " << *wsk2_liczba2 << endl;

    cin >> *wsk2_liczba2;

    cout << "==== PO ZMIANIE ====" << endl;
    cout << "Wartosc zmiennej: " << liczba2 << endl;
    cout << "Adres zmiennej: " << &liczba2 << endl;
    cout << "Wartosc wskaznika: " << *wsk_liczba2 << endl;
    cout << "Adres wskaznika: " << wsk_liczba2 << endl;
    cout << "Wartosc drugiego wskaznika: " << *wsk2_liczba2 << endl;
    cout << "Adres drugiego wskzanika: " << wsk2_liczba2 << endl;

    //int *wsk3;
    //int *wsk4;
    int *wsk3 = new int;
    int *wsk4 = new int(15);
    cout << "Wartosc wsk3: " << *wsk3 << endl;
    cout << "Wartosc wsk4: " << *wsk4 << endl;

    delete wsk3;
    delete wsk4;

    cout << "Wartosc wsk3: " << *wsk3 << endl;
    cout << "Wartosc wsk4: " << *wsk4 << endl;

    // Tablice i wska�niki
    int tabFor[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
    int *wskTab = tabFor;
    //int *wskTab = &tabFor[0];

    for(int i = 0; i < 15; i++)
    {
        cout << *wskTab + i << " ";
    }

}






