#include <iostream>

using namespace std;

class Figura {
public:
    string Kolor;
    virtual void Rysuj()
    {
        cout << "Kolor figury: " << Kolor << endl;
    }
};

class Prostokat : public Figura {
public:
    float Wysokosc;
    float Szerokosc;

    void Rysuj()
    {
        cout << "Rysowanie prostokatu o wymiarach: "
            << Wysokosc << "x" << Szerokosc
            << " i kolorze: " << Kolor << endl;
    }
};

class Kolo : public Figura {
public:
    float Promien;

    void Rysuj()
    {
        cout << "Rysowanie kola o promieniu "
            << Promien << " i kolorze: " << Kolor << endl;
    }

};

int main()
{
    Prostokat p1 = Prostokat();
    p1.Szerokosc = 20.0;
    p1.Wysokosc = 30.0;
    p1.Kolor = "Czerowny";
    p1.Rysuj();

    Kolo k1 = Kolo();
    k1.Promien = 25.7;
    k1.Kolor = "Niebieski";
    k1.Rysuj();

    Figura figury[] = {p1, k1};

    for(int i = 0; i < 2; i++)
    {
        figury[i].Rysuj();
    }
    return 0;
}
