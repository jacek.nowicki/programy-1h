#include <iostream>

using namespace std;

class IWychowawca
{
public:
    virtual void wyswietlKlase() = 0;
};

class Pracownik {
public:
    string imie;
    string nazwisko;

    virtual void wyswietlDane() = 0;
};

class Nauczyciel : public Pracownik, public IWychowawca {
public:
    string klasa;

    Nauczyciel(string imie, string nazwisko, string klasa)
    {
        this->imie = imie;
        this->nazwisko = nazwisko;
        this->klasa = klasa;
    }

    void wyswietlDane() {
        cout << "Nauczyciel " << imie << " " << nazwisko << "\n";
    }

    void wyswietlKlase() {
        cout << "Nauczyciel jest wychowawca klasy " << klasa << endl;
    }
};

class Uczen : public Pracownik {
public:
    Uczen(string imie, string nazwisko)
    {
        this->imie = imie;
        this->nazwisko = nazwisko;
    }

    void wyswietlDane() {
        cout << "Uczen " << imie << " " << nazwisko << "\n";
    }
};

int main()
{
    // Utworzenie obiekt�w nauczyciel oraz ucze�
    Nauczyciel *nauczyciel = new Nauczyciel("Jan", "Nowak", "2D");
    Uczen *uczen = new Uczen("Anna", "Kowalska");

    // Utworzenie tablicy przechowuj�cej obiekty typu pracownik
    Pracownik **tablica = new Pracownik*[2];
    tablica[0] = nauczyciel;
    tablica[1] = uczen;

    for (int i = 0; i < 2; i++)
    {
        tablica[i]->wyswietlDane();
    }

    // Operacje na obiekcie nauczyciel jako typie IWychowawca
    IWychowawca *wychowawca = nauczyciel;
    wychowawca->wyswietlKlase();

    system("pause");
    return 0;
}
