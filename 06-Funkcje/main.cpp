#include <iostream>
#include <string>
#include <math.h>

using namespace std;

float ObliczPredkosc(float s, float t)
{
    float v = s / t;
    return v;
}

int WczytajInt()
{
    string wczytaneDane;
    cin >> wczytaneDane;

    int liczba = stoi(wczytaneDane);
    return liczba;
}

void WyswietlNapis(string napis)
{
    cout << napis << endl;
}

int main()
{
    float dystans, czas;

    /*cout << "Podaj dystans [m]: ";
    cin >> dystans;
    cout << "Podaj czas [s]: ";
    cin >> czas;

    float predkosc = ObliczPredkosc(dystans, czas);
    cout << "Predkosc to: " << predkosc << " m/s" << endl;*/

    int wartosc = WczytajInt();
    cout << "Wczytana liczba to: " << pow(wartosc, 2) << endl;



    return 0;
}
