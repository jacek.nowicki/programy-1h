#include <iostream>

using namespace std;

class Pracownik {
public:
    virtual void wyswietlDane() {
        cout << "Pracownik \n";
    }
};

class Nauczyciel : public Pracownik {
public:
    void wyswietlDane() {
        cout << "Nauczyciel \n";
    }
};

class Uczen : public Pracownik {
public:
    void wyswietlDane() {
        cout << "Uczen \n";
    }
};

int main()
{
    //Pracownik *pracownik = new Nauczyciel();
    //Nauczyciel *nauczyciel = new Nauczyciel();

    //pracownik->wyswietlDane();
    //nauczyciel->wyswietlDane();

    Pracownik **tablica = new Pracownik*[2];
    tablica[0] = new Nauczyciel();
    tablica[1] = new Uczen();

    for (int i = 0; i < 2; i++)
    {
        tablica[i]->wyswietlDane();
    }

    system("pause");
    return 0;
}
