#include <iostream>
#include "matematyka.hpp"

using namespace std;

int main()
{
    int a, b;

    cout << "Podaj a: ";
    cin >> a;
    cout << "Podaj b: ";
    cin >> b;

    cout << "Dodawanie: " << dodawanie(a, b) << endl;
    cout << "Odejmowanie: " << odejmowanie(a, b) << endl;
    cout << "Dzielenie: " << dzielenie(a, b) << endl;
    cout << "Mnozenie: " << mnozenie(a, b) << endl;

    Zespolona x = Zespolona(1, 2);
    x.wyswietl();

    return 0;
}
