#ifndef matematyka_hpp
#define matematyka_hpp

int dodawanie(int a, int b);
int odejmowanie(int a, int b);
int dzielenie(int a, int b);
int mnozenie(int a, int b);

class Zespolona
{
public:
    double re;
    double im;

    Zespolona(double re, double im);

    void wyswietl();
};

#endif // newFile
