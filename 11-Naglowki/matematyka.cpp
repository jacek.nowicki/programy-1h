#include <iostream>
#include "matematyka.hpp"


int dodawanie(int a, int b)
{
    return a + b;
}

int odejmowanie(int a, int b)
{
    return a - b;
}

int dzielenie(int a, int b)
{
    return a / b;
}

int mnozenie(int a, int b)
{
    return a * b;
}


Zespolona::Zespolona(double re, double im)
{
    this->re = re;
    this->im = im;
}

void Zespolona::wyswietl()
{
    std::cout << re << " " << im << std::endl;
}
